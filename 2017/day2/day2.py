TEST = '''5 1 9 5
7 5 3
2 4 6 8'''


def part_a(filename):
    total = 0
    with open(filename, 'r') as f:
        sheet = f.readlines()

    for line in sheet:
        a = [int(n) for n in line.rstrip('\n').split('\t')]
        total += max(a) - min(a)
    return total

# print(part_a('input.txt'))

def modulo_line(line):
    for idx1, n1 in enumerate(line):
        for idx2, n2 in enumerate(line[idx1 + 1:]):
            if (n1 % n2 == 0):
                return n1 // n2
            elif (n2 % n1 == 0):
                return n2 // n1

def part_b(filename):
    total = 0
    with open(filename, 'r') as f:
        sheet = f.readlines()

    for line in sheet:
        a = [int(n) for n in line.rstrip('\n').split('\t')]
        total += modulo_line(a)
    return total

print(part_b('input.txt'))